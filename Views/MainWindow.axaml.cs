using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using NAudio.Wave;
using System;
using System.IO;

namespace AT4.Views
{
    public partial class MainWindow : Window
    {
        private readonly Control _bottomControl;
        private double _bottomControlLeft;
        private double _bottomControlTop;
        private readonly WaveOutEvent _waveOut = new WaveOutEvent();
        private const double MoveStep = 30.0;

        public MainWindow()
        {
            InitializeComponent();
            this.KeyDown += MainWindow_KeyDown;
            _bottomControl = this.FindControl<Control>("BottomControl");
            DataContext = this;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left:
                    MoveControl(-MoveStep, 0);
                    break;
                case Key.Right:
                    MoveControl(MoveStep, 0);
                    break;
                case Key.Up:
                    MoveControl(0, -MoveStep);
                    break;
                case Key.Down:
                    MoveControl(0, MoveStep);
                    break;
            }

            PlaySound("AT4/Assets/Sounds/bubbles.wav");
        }

        private void MoveControl(double offsetX, double offsetY)
        {
            _bottomControlLeft += offsetX;
            _bottomControlTop += offsetY;
            UpdateControlPosition();
        }

        private void UpdateControlPosition()
        {
            Canvas.SetLeft(_bottomControl, _bottomControlLeft);
            Canvas.SetTop(_bottomControl, _bottomControlTop);
        }

        private void PlaySound(string soundFile)
        {
            try
            {
                if (!File.Exists(soundFile))
                    throw new FileNotFoundException("Sound file not found.", soundFile);

                using var audioFile = new AudioFileReader(soundFile);
                _waveOut.Init(audioFile);
                _waveOut.Play();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error playing sound: {ex.Message}");
            }
        }
    }
}

